<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>สร้างเอกสาร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>จัดการเอกสาร</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>


                <div class="row">
                    <div class="col">
                        <section class="card card-modern card-big-info">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-2 col-xl-2">
                                        <i class="card-big-info-icon bx bx-file"></i>
                                        <h2 class="card-big-info-title">สร้างเอกสาร</h2>
                                        <p class="card-big-info-desc"></p>
                                    </div>

                                    <div class="col-lg-10 col-xl-10">
                                        <div class="form-group row align-items-center">
                                            <label class="col-lg-3 control-label text-lg-right pt-2 text-5">ประเภทของเอกสาร <span class="required">*</span></label>
                                            <div class="col-lg-6">
                                                <select id="" class="form-control select-box-tab">
                                                    <option value="0">ประเภทเอกสาร</option>
                                                    <option value="1-1">ราคากลาง</option>
                                                    <option value="2-2">บัญชีรายการพัสดุที่ต้องการซื้อ/จ้าง</option>
                                                    <option value="3-3">เอกสารข้อกำหนดคุณลักษณะพัสดุที่ต้องการซื้อ
                                                        <!--  / จ้าง สำหรับวิธี eBidding วิธีคัดเลือก และวิธีเฉพาะเจาะจงกรณีวงเงินเกิน 5 แสนบาท -->
                                                    </option>
                                                    <option value="4-4">ใบคำขอการจัดซื้อ / จัดจ้าง</option>
                                                    <option value="5-5">ใบเงินยืมทดรอง</option>
                                                    <option value="6-6" selected>ใบเคลียร์เงินทดรองจ่าย</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div id="tab-6-6" class="select-tab ">
                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">1</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">เลขจองงบประมาณ</label>
                                                    <input type="text" class="form-control">
                                                    <small class="notice">กรุณากดปุ่ม Enter เมื่อกรอกข้อมูลเลขจองงบประมาณเรียบร้อยแล้ว</small>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3 offset-lg-1">
                                                    <label class="title-label">เลขจองงบประมาณ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <label class="title-label">จำนวนเงิน</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <label class="title-label">เลขที่โครงการ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3 offset-lg-1">
                                                    <label class="title-label">ชื่อโครงการ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <label class="title-label">ฝ่ายเจ้าของโครงการ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <label class="title-label">แหล่งเงิน</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-3 offset-lg-1">
                                                    <label class="title-label">ปีงบประมาณ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">2</span></div>
                                                <div class="col-lg-3 col-xl-3">
                                                    <label class="title-label">แผนก</label>
                                                    <input type="text" class="form-control" disabled value="ฝ่ายบริหารกลาง">
                                                </div>
                                                <div class="col-lg-3 col-xl-3">
                                                    <label class="title-label">กลุ่ม</label>
                                                    <input type="text" class="form-control" disabled value="">
                                                </div>
                                                <div class="col-lg-3 col-xl-3">
                                                    <label class="title-label">
                                                        งาน/ห้องปฏิบัติการ</label>
                                                    <input type="text" class="form-control" disabled value="">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">3</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">เรื่อง <span class="required">*</span></label>
                                                    <input type="text" class="form-control" placeholder="เรื่อง">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">4</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">บันทึกข้อความเลขที่ <span class="required">*</span></label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">5</span></div>
                                                <div class="col-lg-11 col-xl-11">
                                                    <table class="table  table-striped mb-0">
                                                        <thead>
                                                            <tr class="head-table">
                                                                <th class="center " width="20%">รายการ</th>
                                                                <th class="center " width="20%">จำนวน</th>
                                                                <th class="center ">หน่วยนับ</th>
                                                                <th class="center ">อัตราต่อหน่วย</th>
                                                                <th class="center ">รวม</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <select data-plugin-selectTwo class="form-control populate">
                                                                        <option value="" disabled></option>
                                                                        <option value=""></option>
                                                                    </select>
                                                                </td>
                                                                <td><input type="text" class="form-control"></td>
                                                                <td><input type="text" class="form-control"></td>
                                                                <td><input type="text" class="form-control"></td>
                                                                <td><input type="text" class="form-control" disabled></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <a type="button" class="mb-1 mt-3 mr-1 btn btn-primary btn-px-4"><i class="fas fa-plus mr-2"></i> เพิ่มรายการ</a>
                                                    <hr>
                                                    <div class="row ">
                                                        <div class="col-lg-10 control-label text-lg-right">
                                                            <h5>รวมทั้งหมด</h5>
                                                        </div>
                                                        <div class="col-lg-2 control-label text-lg-right">
                                                            <h5>0.00</h5>
                                                        </div>
                                                        <div class="col-lg-10 control-label text-lg-right">
                                                            <h5>วงเงินยืมทดรอง
                                                            </h5>
                                                        </div>
                                                        <div class="col-lg-2 control-label text-lg-right">
                                                            <h5>0.00</h5>
                                                        </div>
                                                        <div class="col-lg-10 control-label text-lg-right">
                                                            <h5>จ่ายเพิ่ม / (ส่งคืน)
                                                            </h5>
                                                        </div>
                                                        <div class="col-lg-2 control-label text-lg-right">
                                                            <h5>0.00</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row align-items-center">
                                                <div class="col-lg-1"><span class="badge-title badge ">6</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">อัพโหลดไฟล์แนบ</label>
                                                    <form action="/upload" class="dropzone dz-square dz-clickable" id="dropzone-example">
                                                        <div class="dz-default dz-message"><button class="dz-button" type="button">Drop files here to upload</button></div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">7</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">จัดการสายอนุมัติ <span class="required">*</span></label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" selected>เลือกรายการสายอนุมัติ</option>
                                                        <option value="">test</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">8</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">จัดการสายการอนุมัติ ผู้บริหาร <span class="required">*</span></label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" selected>เลือกรายการอนุมัติ ผู้บริหาร</option>
                                                        <option value="">test</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>




                                </div>
                            </div>
                    </div>
            </section>
        </div>
        </div>


        <div class="action-buttons-fixed">
            <div class="row action-buttons">
                <div class="col-12 col-md-auto">
                    <button type="submit" class="submit-button btn btn-primary btn-px-4 py-3 d-flex align-items-center font-weight-semibold line-height-1" data-loading-text="Loading...">
                        <i class="bx bx-save text-4 mr-2"></i> บันทึกข้อมูล
                    </button>
                </div>
                <div class="col-12 col-md-auto px-md-0 mt-3 mt-md-0">
                    <a href="#" class="cancel-button btn btn-light btn-px-4 py-3 border font-weight-semibold text-color-dark text-3"><i class="far fa-eye"></i> ดูตัวอย่าง</a>
                </div>
            </div>
        </div>
    </section>

    </div>


    </section>
    <?php include 'include/inc-script.php'; ?>



</body>

</html>