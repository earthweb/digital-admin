<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>ข่าวใหม่</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ข่าวใหม่</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col-lg-6 col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-3 d-flex">
                                    <h4 class="mr-3">สร้างโพสต์</h4>
                                    <div class="col-3">
                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fas fa-globe"></i> สาธารณะ <span class="caret"></span></button>
                                        <div class="dropdown-menu" role="menu">
                                            <a class="dropdown-item text-1" href="#">สาธารณะ</a>
                                            <a href="#modalAnim" class="modal-with-zoom-anim dropdown-item text-1">แผนกที่เกี่ยวข้อง</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
                                    <section class="card">
                                        <header class="card-header">
                                            <h2 class="card-title">แผนกที่เกี่ยวข้อง</h2>
                                        </header>
                                        <div class="card-body">
                                            <form class="p-3">
                                                <div class="row">
                                                    <div class="col-lg-12 mb-2">
                                                        <label class="title-label">
                                                            แผนก</label>
                                                        <div class="">
                                                            <select id="" data-plugin-selectTwo class="form-control populate" multiple>
                                                                <option value="1">แผนก 1</option>
                                                                <option value="2">แผนก 2</option>
                                                                <option value="3">แผนก 3</option>
                                                                <option value="4">แผนก 4</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button class="btn btn-primary w-150">ตกลง</button>
                                                    <button class="btn btn-default modal-dismiss w-150">ยกเลิก</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </div>

                                <div class="d-flex align-items-center">
                                    <div>
                                        <img src="img/!logged-user.jpg" alt="" class="rounded-circle mr-3" data-lock-picture="img/!logged-user.jpg" height="60">
                                    </div>
                                    <section class="simple-compose-box w-100">
                                        <form method="get" action="/">
                                            <textarea name="message-text" data-plugin-textarea-autosize placeholder="" rows="2"></textarea>
                                        </form>
                                        <div class="compose-box-footer">
                                            <ul class="compose-toolbar">
                                                <li>
                                                    <a href="#"><i class="fas fa-paperclip"></i></a>
                                                </li>
                                            </ul>
                                            <ul class="compose-btn">
                                                <li>
                                                    <a href="#" class="btn btn-primary btn-xs">Post</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </section>
                                </div>
                                <hr>

                                <h4 class="mb-3 pt-2"><button class="btn text-main">โพสต์ทั้งหมด</button>/<button class="btn">โพสต์ที่เกี่ยวข้อง</button> </h4>

                                <div class="timeline timeline-simple mt-3 mb-3">
                                    <div class="tm-body">
                                        <div class="tm-title">
                                            <h5 class="m-0 pt-2 pb-2 text-uppercase">มีนาคม 2565</h5>
                                        </div>
                                        <ol class="tm-items">
                                            <li>
                                                <div class="tm-box">
                                                    <div class="post-item">
                                                        <div class="user-post">
                                                            <div class="d-flex align-items-center">
                                                                <img src="img/!logged-user.jpg" alt="" class="rounded-circle mr-2" data-lock-picture="img/!logged-user.jpg" height="40">
                                                                <p class="text-dark mb-0">
                                                                    ชื่อ-นามสกุล
                                                                    <small class="text-muted mb-0">1 วัน, 7 ชั่วโมง ที่แล้ว</small>
                                                                </p>
                                                            </div>
                                                            <button type="button" class="btn-more mb-1 mt-1 mr-1 btn dropdown-toggle" data-toggle="dropdown"><i class="fas fa-ellipsis-h "></i></button>
                                                            <div class="dropdown-menu" role="menu">
                                                                <a class="dropdown-item text-1" href="#">ลบ</a>
                                                                <a class="dropdown-item text-1" href="#">แก้ไข</a>
                                                            </div>
                                                        </div>
                                                        <div class="detail-post mb-2">
                                                            <p>
                                                                It's awesome when we find a good solution for our projects, Porto Admin is <span class="text-primary">#awesome</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <section class="simple-compose-box mt-4 w-100">
                                                        <form method="get" action="/">
                                                            <textarea name="message-text" data-plugin-textarea-autosize placeholder="" rows="1"></textarea>
                                                        </form>
                                                        <div class="compose-box-footer">
                                                            <ul class="compose-toolbar">
                                                                <li>
                                                                    <a href="#"><i class="fas fa-paperclip"></i></a>
                                                                </li>
                                                            </ul>
                                                            <ul class="compose-btn">
                                                                <li>
                                                                    <a href="#" class="btn btn-primary btn-xs">Reply</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </section>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="tm-box">
                                                    <div class="post-item">
                                                        <div class="user-post">
                                                            <div class="d-flex align-items-center">
                                                                <img src="img/!logged-user.jpg" alt="" class="rounded-circle mr-2" data-lock-picture="img/!logged-user.jpg" height="40">
                                                                <p class="text-dark mb-0">
                                                                    ผู้ใช้อื่น
                                                                    <small class="text-muted mb-0">6 วัน, 2 ชั่วโมง ที่แล้ว</small>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="detail-post mb-2">
                                                            <p>
                                                                What is your biggest developer pain point?
                                                            </p>
                                                        </div>
                                                        <div class="comment-other">
                                                            <div class="list-comment">
                                                                <div class="user-post">
                                                                    <div class="d-flex align-items-center">
                                                                        <img src="img/!logged-user.jpg" alt="" class="rounded-circle mr-2" data-lock-picture="img/!logged-user.jpg" height="40">
                                                                        <p class="text-dark mb-0">
                                                                            ชื่อ-นามสกุล
                                                                            <small class="text-muted mb-0">6 วัน, 2 ชั่วโมง ที่แล้ว</small>
                                                                        </p>
                                                                    </div>
                                                                    <button type="button" class="btn-more mb-1 mt-1 mr-1 btn dropdown-toggle" data-toggle="dropdown"><i class="fas fa-ellipsis-h "></i></button>
                                                                    <div class="dropdown-menu" role="menu">
                                                                        <a class="dropdown-item text-1" href="#">ลบ</a>
                                                                        <a class="dropdown-item text-1" href="#">แก้ไข</a>
                                                                    </div>
                                                                </div>
                                                                <div class="detail-post mb-2">
                                                                    <p>
                                                                        What is your biggest developer pain point?
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="list-comment">
                                                                <div class="user-post">
                                                                    <div class="d-flex align-items-center">
                                                                        <img src="img/!logged-user.jpg" alt="" class="rounded-circle mr-2" data-lock-picture="img/!logged-user.jpg" height="40">
                                                                        <p class="text-dark mb-0">
                                                                            ชื่อ-นามสกุล
                                                                            <small class="text-muted mb-0">6 วัน, 2 ชั่วโมง ที่แล้ว</small>
                                                                        </p>
                                                                    </div>
                                                                    <button type="button" class="btn-more mb-1 mt-1 mr-1 btn dropdown-toggle" data-toggle="dropdown"><i class="fas fa-ellipsis-h "></i></button>
                                                                    <div class="dropdown-menu" role="menu">
                                                                        <a class="dropdown-item text-1" href="#">ลบ</a>
                                                                        <a class="dropdown-item text-1" href="#">แก้ไข</a>
                                                                    </div>
                                                                </div>
                                                                <div class="detail-post mb-2">
                                                                    <p>
                                                                        What is your biggest developer pain point?
                                                                    </p>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <section class="simple-compose-box mt-4 w-100">
                                                        <form method="get" action="/">
                                                            <textarea name="message-text" data-plugin-textarea-autosize placeholder="" rows="1"></textarea>
                                                        </form>
                                                        <div class="compose-box-footer">
                                                            <ul class="compose-toolbar">
                                                                <li>
                                                                    <a href="#"><i class="fas fa-paperclip"></i></a>
                                                                </li>
                                                            </ul>
                                                            <ul class="compose-btn">
                                                                <li>
                                                                    <a href="#" class="btn btn-primary btn-xs">Reply</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </section>

                                                </div>
                                            </li>
                                            <li>
                                                <div class="tm-box">
                                                    <div class="post-item">
                                                        <div class="user-post">
                                                            <div class="d-flex align-items-center">
                                                                <img src="img/!logged-user.jpg" alt="" class="rounded-circle mr-2" data-lock-picture="img/!logged-user.jpg" height="40">
                                                                <p class="text-dark mb-0">
                                                                    ผู้ใช้อื่น
                                                                    <small class="text-muted mb-0">6 วัน, 2 ชั่วโมง ที่แล้ว</small>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="detail-post mb-2">
                                                            <p>
                                                                What is your biggest developer pain point?
                                                            </p>
                                                        </div>
                                                        <div class="thumbnail-gallery">
                                                            <a class="img-thumbnail lightbox" href="img/projects/project-4.jpg" data-plugin-options='{ "type":"image" }'>
                                                                <img class="img-fluid" width="215" src="img/projects/project-4.jpg">
                                                                <span class="zoom">
                                                                    <i class="bx bx-search"></i>
                                                                </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <section class="simple-compose-box mt-4 w-100">
                                                        <form method="get" action="/">
                                                            <textarea name="message-text" data-plugin-textarea-autosize placeholder="" rows="1"></textarea>
                                                        </form>
                                                        <div class="compose-box-footer">
                                                            <ul class="compose-toolbar">
                                                                <li>
                                                                    <a href="#"><i class="fas fa-paperclip"></i></a>
                                                                </li>
                                                            </ul>
                                                            <ul class="compose-btn">
                                                                <li>
                                                                    <a href="#" class="btn btn-primary btn-xs">Reply</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </section>
                                                </div>
                                            </li>
                                        </ol>
                                        <div class="tm-title">
                                            <h5 class="m-0 pt-2 pb-2 text-uppercase">กุมภาพันธ์ 2565</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-3">
                                    <h4 class="mr-3">ประกาศ</h4>
                                </div>
                                <div class="search">
                                    <input type="text" class="form-control" placeholder="ค้นหา">
                                </div>
                                <hr>
                                <div class="scrollable scroll-news" data-plugin-scrollable>
                                    <div class="scrollable-content">
                                        <ul class="list-approval">
                                            <?php
                                            for ($x = 0; $x <= 5; $x++) { ?>
                                                <li>
                                                    <a href="#modalAnim2" class="modal-with-zoom-anim">
                                                        <h5><span class="badge bg-info text-white py-2">แผนก A</span>&nbsp;<small>11/04/2565</small> </h5>
                                                        <h6>
                                                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquid, quam enim blanditiis earum, qui quod quas, neque veniam est officia excepturi autem minima? Adipisci minus explicabo voluptates ad perferendis repudiandae!
                                                            <h6>
                                                    </a>
                                                </li>
                                            <?php
                                            }
                                            ?>

                                        </ul>
                                    </div>
                                </div>
                                <div id="modalAnim2" class="zoom-anim-dialog modal-block modal-block-lg modal-block-primary mfp-hide">
                                    <section class="card scrollable scroll-news">
                                        <div class="card-body scrollable scroll-pdf" data-plugin-scrollable>
                                            <img class="img-thumbnail mb-2 w-100" src="img/projects/project-1.jpg" alt="">
                                            <img class="img-thumbnail mb-2 w-100" src="img/projects/project-1.jpg" alt="">
                                        </div>
                                        <footer class="card-footer action-buttons">
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button class="btn btn-primary w-150"><i class="fas fa-print text-white"></i> ปริ้น</button>
                                                    <button class="btn btn-default modal-dismiss w-10">ปิด</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>




            </section>

        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>



</body>

</html>