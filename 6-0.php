<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดการสายอนุมัติผู้บริหาร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col">
                        <section class="card">
                            <header class="card-header">
                                <h2 class="card-title">จัดการสายอนุมัติผู้บริหาร</h2>
                            </header>
                            <div class="card-body">
                                <div class="fliter-group row">
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-3 mb-2">
                                                <label class="title-label">ประเภทเอกสาร</label>
                                                <select data-plugin-selectTwo class="form-control populate">
                                                    <option value="0" disabled selected>ประเภทเอกสาร</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3 mb-2">
                                                <label class="title-label">ผู้อนุมัติ</label>
                                                <select data-plugin-selectTwo class="form-control populate">
                                                    <option value="0" disabled selected>ผู้อนุมัติ</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3 mb-2">
                                                <button type="button" class="mb-1 mt-4 mr-1 btn btn-info btn-clear btn-px-4 py-2">ล้าง</button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-3 text-right"><a type="button" href="5-1.php" class=" mb-1 mt-4 mr-1 btn btn-primary btn-px-4 py-3"><i class="fas fa-plus mr-2"></i> สร้างสายอนุมัติผู้บริหาร</a></div>

                                </div>
                                <hr>
                                <table class="table  table-striped mb-0" id="datatable-default">
                                    <thead>
                                        <tr class="head-table">
                                            <th class="center" width="10%">ลำดับ</th>
                                            <th>ประเภทเอกสาร</th>
                                            <th>ชื่อ</th>
                                            <th>ช่วงจำนวน</th>
                                            <th class="center " width="10%">แก้ไข</th>
                                            <th class="center " width="10%">ลบ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="center ">1</td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>


                                            <td class="actions center">
                                                <a href="#" class="hidden on-editing save-row"><i class="fas fa-save"></i></a>
                                                <a href="#" class="hidden on-editing cancel-row"><i class="fas fa-times"></i></a>
                                                <a href="#" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                            </td>
                                            <td class="actions center">
                                                <a href="#" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </section>

                    </div>
                </div>





            </section>
        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>