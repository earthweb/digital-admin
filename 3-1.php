<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>อัปโหลดเอกสาร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>จัดการเอกสาร</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>


                <div class="row">
                    <div class="col">
                        <section class="card card-modern card-big-info">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-2 col-xl-2">
                                        <i class="card-big-info-icon bx bx-file"></i>
                                        <h2 class="card-big-info-title">อัปโหลดเอกสาร</h2>
                                        <p class="card-big-info-desc"></p>
                                    </div>
                                    <div class="col-lg-10 col-xl-10">
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">1</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">ประเภทเอกสาร</label>
                                                <select data-plugin-selectTwo class="form-control populate placeholder" data-plugin-options='{ "placeholder": "ประเภทเอกสาร", "allowClear": false }'>
                                                    <option value="0" disabled selected>ประเภทเอกสาร</option>
                                                    <option value="0">ใบกำกับภาษี</option>
                                                    <option value="1">ใบแจ้งหนี้</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 col-xl-4">
                                                <label class="title-label">Template สำหรับเอกสารอัปโหลด</label>
                                                <select data-plugin-selectTwo class="form-control populate placeholder" data-plugin-options='{ "placeholder": "Template สำหรับเอกสารอัปโหลด", "allowClear": false }'>
                                                    <option value="0" disabled selected>Template สำหรับเอกสารอัปโหลด</option>
                                                    <option value="0">1</option>
                                                    <option value="1">2</option>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">2</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">แผนก</label>
                                                <select class="form-control form-control-modern" name="">
                                                    <option value="percentage" selected>เลือกแผนก</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">3</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">จัดการสายอนุมัติ</label>
                                                <select class="select-box-tab form-control form-control-modern" name="">
                                                    <option value="" selected>เลือกสายการอนุมัติ</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                    <option value="custom1">Custom</option>
                                                </select>

                                                <div id="tab-custom1" class="select-tab mt-3">
                                                    <div class="form-group row">
                                                        <label class="title-label col-lg-2 control-label text-lg-right pt-2">ลำดับที่ 1</label>
                                                        <div class="col-lg-7">
                                                            <select multiple data-plugin-selectTwo class="form-control populate">
                                                                <option value="">ชื่อ นามสกุล</option>
                                                                <option value="">ชื่อ นามสกุล</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-sm btn-info">+</button>
                                                            <button type="button" disabled class="mb-1 mt-1 mr-1 btn btn-sm btn-danger">-</button>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">4</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">อ้างอิง</label>
                                                <select multiple data-plugin-selectTwo class="form-control populate">
                                                    <option value="">อ้างอิง - 1 </option>
                                                    <option value="">อ้างอิง - 2 </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">5</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">สำเนาถึง</label>
                                                <select multiple data-plugin-selectTwo class="form-control populate">
                                                    <option value="">1</option>
                                                    <option value="">1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">6</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">เรื่อง</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">7</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">ประเภทของเลขที่เอกสาร</label>
                                                <select class="select-box-tab form-control form-control-modern">
                                                    <option value="1">สร้างเลขที่เอกสารแบบอัตโนมัติ</option>
                                                    <option value="" selected>สร้างเลขที่เอกสารด้วยตัวเอง</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label class="title-label">สร้างเลขที่เอกสารด้วยตัวเอง</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">8</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">เลือกแผนกที่ต้องการประกาศ</label>
                                                <select class="form-control" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "maxHeight": 200, "enableCaseInsensitiveFiltering": true }' id="ms_example6">
                                                    <optgroup label="title group">
                                                        <option value="analysis">1</option>
                                                        <option value="algebra">2</option>
                                                        <option value="discrete">3</option>
                                                        <option value="numerical">4</option>
                                                        <option value="probability">5</option>
                                                    </optgroup>
                                                    <optgroup label="title group">
                                                        <option value="programming">1</option>
                                                        <option value="automata">2</option>
                                                        <option value="complexity">3</option>
                                                        <option value="software">4</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">9</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">ต้องการการตอบรับ</label>
                                                <select class="form-control form-control-modern">
                                                    <option value="1" selected>ไม่ต้องการ</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <section class="card card-modern card-big-info">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-2 col-xl-2">
                                        <i class="card-big-info-icon fas fa-file-upload"></i>
                                        <h2 class="card-big-info-title">อัปโหลดไฟล์</h2>
                                        <p class="card-big-info-desc"></p>
                                    </div>
                                    <div class="col-lg-10 col-xl-10">
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">10</span></div>
                                            <div class="col-lg-8 col-xl-8">
                                                <label class="title-label">อัปโหลด PDF</label>
                                                <form action="/upload" class="dropzone dz-square" id="dropzone-example"></form>
                                                <div class="notice  mt-2 mb-2">
                                                    *เฉพาะอัพโหลดเอกสาร(PDF ขนาด A4)
                                                </div>
                                                <div class="block-pdf">
                                                    <hr>
                                                    <div class="menu-mn-pdf my-2 d-flex justify-content-center">
                                                        <button type="button" class="mb-1 mt-1 mx-2 btn btn-success btn-etc"><i class="fas fa-file-signature mr-2"></i>ลายน้ำ</button>
                                                        <button type="button" class="mb-1 mt-1 mx-2 btn btn-yellow"><i class="fas fa-calendar-alt mr-2"></i>วันที่</button>
                                                        <button type="button" class="mb-1 mt-1 mx-2 btn btn-warning btn-etc"><i class="fas fa-file-alt mr-2"></i>เลขที่เอกสาร</button>
                                                        <button type="button" class="mb-1 mt-1 mx-2 btn btn-danger btn-etc"><i class="fas fa-font mr-2"></i>เพิ่มข้อความ</button>
                                                        <button type="button" class="mb-1 mt-1 mx-2 btn btn-grey"><i class="fas fa-dumpster mr-2"></i>ล้าง</button>
                                                    </div>
                                                    <hr>
                                                    <div class="owl-carousel owl-theme" data-plugin-carousel data-plugin-options='{ "dots": false, "nav": true, "items": 1 }'>
                                                        <div class="item"><img class="img-thumbnail" src="img/projects/project-1.jpg" alt=""></div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">11</span></div>
                                            <div class="col-lg-8 col-xl-8">
                                                <label class="title-label">อัพโหลดไฟล์แนบ</label>
                                                <form action="/upload" class="dropzone dz-square" id="dropzone-example"></form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>



                <div class="action-buttons-fixed">
                    <div class="row action-buttons">
                        <div class="col-12 col-md-auto">
                            <button type="submit" class="submit-button btn btn-primary btn-px-4 py-3 d-flex align-items-center font-weight-semibold line-height-1" data-loading-text="Loading...">
                                <i class="bx bx-save text-4 mr-2"></i> บันทึกข้อมูล
                            </button>
                        </div>
                        <div class="col-12 col-md-auto px-md-0 mt-3 mt-md-0">
                            <a href="#" class="cancel-button btn btn-light btn-px-4 py-3 border font-weight-semibold text-color-dark text-3">ยกเลิก</a>
                        </div>
                    </div>
                </div>
            </section>

        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>



</body>

</html>