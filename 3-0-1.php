<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>รอการอนุมัติ</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>


                <div class="row">
                    <div class="col-lg-2">
                        <div class="mb-3">
                            <a href="#" onclick="history.back()" type="button" class="cancel-button btn btn-light btn-px-4 py-3 border font-weight-semibold text-color-dark text-3"><i class="fas fa-arrow-left"></i> ย้อนกลับ</a>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="mb-3">
                            <h5>65PV03000195 บริษัท สยามแมคอินทอช จำกัด</h5>
                            <h6>เลขที่เอกสาร: <span class="text-main">xxxxxxxxxxxxxxxxxx</span>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="row">

                    <div class="col-lg-7">
                        <div class="wizard-progress my-2">
                            <ul class="nav wizard-steps step-approval">
                                <li class="nav-item active">
                                    <a href="#" class="nav-link"><span><i class="fas fa-check"></i></span>พนักงาน 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><span>2</span>พนักงาน 2</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><span>3</span>พนักงาน 3</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><span>4</span>พนักงาน 4</a>
                                </li>
                            </ul>
                        </div>
                        <hr>
                        <div class="block-pdf">
                            <div class="row justify-content-center">
                                <div class="col-lg-10">
                                    <img class="img-thumbnail mb-2 w-100" src="img/projects/project-1.jpg" alt="">
                                    <img class="img-thumbnail mb-2 w-100" src="img/projects/project-1.jpg" alt="">
                                    <img class="img-thumbnail mb-2 w-100" src="img/projects/project-1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">

                        <div class="tabs">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#tab-2" data-toggle="tab" class="text-center">แสดงความคิดเห็น</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#tab-3" data-toggle="tab" class="text-center">
                                        ไฟล์แนบ
                                        <span class="badge badge-danger  badge-sm">1</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#tab-4" data-toggle="tab" class="text-center">
                                        เอกสารอ้างอิง
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#tab-5" data-toggle="tab" class="text-center">
                                        ประวัติ
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-2" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="tm-box">
                                                <div class="user-list">
                                                    <div class="d-flex align-items-center">
                                                        <img src="img/!logged-user.jpg" alt="Joseph Doe" height="40" class="rounded-circle" data-lock-picture="img/!logged-user.jpg">
                                                        <span class="mx-1">เกษร ลิมปดาพันธ์
                                                            <small class="text-muted"> 9 ชั่วโมง, 15 นาที ที่แล้ว</small>
                                                        </span>
                                                    </div>
                                                    <div class="comment-item">
                                                        เหตุผลที่ต้องแก้ไข: แก้ไขรายการที่ 3 ให้ถูกต้อง
                                                    </div>
                                                    <div class="comment-item pdf-feedback">
                                                        <span class="badge badge-danger">PDF</span> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint saepe rem laudantium enim voluptates possimus itaque voluptate eligendi, fuga fugiat debitis similique autem, quibusdam soluta accusamus ea dolores accusantium explicabo.
                                                    </div>
                                                </div>
                                                <div class="user-list">
                                                    <div class="d-flex align-items-center">
                                                        <img src="img/!logged-user.jpg" alt="Joseph Doe" height="40" class="rounded-circle" data-lock-picture="img/!logged-user.jpg">
                                                        <span class="mx-1">ชื่อ นามสกุล
                                                            <small class="text-muted"> 9 ชั่วโมง, 15 นาที ที่แล้ว</small>
                                                        </span>
                                                    </div>
                                                    <div class="comment-item">
                                                        เหตุผลที่ต้องแก้ไข: แก้ไขรายการที่ 3 ให้ถูกต้อง
                                                    </div>
                                                    <div class="comment-item pdf-feedback">
                                                        <span class="badge badge-danger">PDF</span> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint saepe rem laudantium enim voluptates possimus itaque voluptate eligendi, fuga fugiat debitis similique autem, quibusdam soluta accusamus ea dolores accusantium explicabo.
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-lg-12">
                                            <div class="block-comment">
                                                <!-- <input tpye="text" class="form-control" rows="1" placeholder="แสดงความคิดเห็น"></input>
                                                <div class="button-comment">
                                                    <button class="btn btn-comment mt-2 add-file"><i class="fas fa-paperclip"></i></button>
                                                    <button class="btn btn-comment mt-2 send-comment"><i class="fas fa-paper-plane"></i></button>
                                                </div> -->
                                                <section class="simple-compose-box mb-3">
                                                    <form method="get" action="/">
                                                        <textarea name="message-text" data-plugin-textarea-autosize placeholder="แสดงความคิดเห็น" rows="2"></textarea>
                                                    </form>
                                                    <div class="compose-box-footer">
                                                        <ul class="compose-toolbar">
                                                            <li>
                                                                <a href="#"><i class="fas fa-paperclip"></i></a>
                                                            </li>
                                                        </ul>
                                                        <ul class="compose-btn">
                                                            <li>
                                                                <a href="#" class="btn btn-primary btn-xs"><i class="fas fa-paper-plane"></i> Post</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div id="tab-3" class="tab-pane">
                                    <div class="tm-box">
                                        <div class="file-list">
                                            <div class="file-item">
                                                <a href="#" class="file-item-info">
                                                    <span class="badge badge-danger">PDF</span>&nbsp;1. ชื่อไฟล์.pdf
                                                </a>
                                                <a href="#" class="">
                                                    <i class="fas fa-download"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-4" class="tab-pane">
                                    <div class="tm-box d-flex justify-content-center align-items-center">
                                        <i class="fas fa-folder-open file-empty"></i>
                                    </div>
                                </div>
                                <div id="tab-5" class="tab-pane">

                                    <div class="tm-box">
                                        <div class="history-list">
                                            <div class="history-item">
                                                <div class="d-flex align-items-center">
                                                    <img src="img/!logged-user.jpg" alt="Joseph Doe" height="40" class="rounded-circle" data-lock-picture="img/!logged-user.jpg">
                                                    <span class="mx-1">เกษร ลิมปดาพันธ์
                                                        <small class="text-muted"> 9 ชั่วโมง, 15 นาที ที่แล้ว</small>
                                                        <div class="text-muted">
                                                            แสดงความคิดเห็นในเอกสารที่ถูกอัพโหลด
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="history-item">
                                                <div class="d-flex align-items-center">
                                                    <img src="img/!logged-user.jpg" alt="Joseph Doe" height="40" class="rounded-circle" data-lock-picture="img/!logged-user.jpg">
                                                    <span class="mx-1">เกษร ลิมปดาพันธ์
                                                        <small class="text-muted"> 9 ชั่วโมง, 15 นาที ที่แล้ว</small>
                                                        <div class="text-muted">
                                                            แก้ไขเอกสารที่ถูกอัพโหลด
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </section>
        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>

</body>

</html>