<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>สร้างเอกสาร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row create-file">
                    <div class="col-lg-3 col-xl-3 pb-2 pb-lg-0 mb-4 mb-lg-0">
                        <a href="3-1.php">
                            <div class="card card-modern">
                                <div class="card-body py-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 text-left text-md-right pr-md-4 mt-4 mt-md-0">
                                            <i class="bx bx-upload icon icon-inline icon-xl bg-secondary rounded-circle text-color-light"></i>
                                        </div>
                                        <div class="col-8 col-md-8">
                                            <h3 class="text-4-1 my-0 mx-2">อัปโหลดเอกสาร</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-xl-3 pb-2 pb-lg-0 mb-4 mb-lg-0">
                        <a href="3-2-1.php">
                            <div class="card card-modern">
                                <div class="card-body py-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 text-left text-md-right pr-md-4 mt-4 mt-md-0">
                                            <i class="bx bx-file-blank icon icon-inline icon-xl bg-primary rounded-circle text-color-light"></i>
                                        </div>
                                        <div class="col-8 col-md-8">
                                            <h3 class="text-4-1 my-0 mx-2">สร้างเอกสาร</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6 mb-3">
                                        <input type="text" class="form-control" placeholder="ค้นหาเอกสาร">
                                    </div>
                                    <div class="col-lg-6 mb-2">
                                        <div class="float-input">
                                            <i class='bx bx-calendar-alt'></i>
                                            <input type="text" data-plugin-datepicker class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <select class="form-control form-control-modern" name="stockStatus">
                                            <option value="percentage" selected="">แผนกทั้งหมด</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <select class="form-control form-control-modern" name="stockStatus">
                                            <option value="percentage" selected="">แสดงเอกสารขออนุมัติทั้งหมด</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row filter-create ">
                    <div class="col">
                        <a href="#">
                            <div class="card card-modern">
                                <div class="card-body py-4 card-active">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 text-left text-md-right pr-md-4 mt-4 mt-md-0">
                                            <i class="bx bx-file icon icon-inline icon-xl "></i>
                                        </div>
                                        <div class="col-8 col-md-8">
                                            <h3 class="text-4-1 my-0 ">ทั้งหมด</h3>
                                            <strong class="text-5 ">1857</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="#">
                            <div class="card card-modern">
                                <div class="card-body py-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 text-left text-md-right pr-md-4 mt-4 mt-md-0">
                                            <i class="bx bx-message-edit icon icon-inline icon-xl "></i>
                                        </div>
                                        <div class="col-8 col-md-8">
                                            <h3 class="text-4-1 my-0 ">ฉบับร่าง</h3>
                                            <strong class="text-5 ">1</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="#">
                            <div class="card card-modern">
                                <div class="card-body py-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 text-left text-md-right pr-md-4 mt-4 mt-md-0">
                                            <i class="bx bx-time icon icon-inline icon-xl "></i>
                                        </div>
                                        <div class="col-8 col-md-8">
                                            <h3 class="text-4-1 my-0 ">รอดำเนินการ</h3>
                                            <strong class="text-5 ">81</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="#">
                            <div class="card card-modern">
                                <div class="card-body py-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 text-left text-md-right pr-md-4 mt-4 mt-md-0">
                                            <i class="bx bx-message-x icon icon-inline icon-xl "></i>
                                        </div>
                                        <div class="col-8 col-md-8">
                                            <h3 class="text-4-1 my-0 text-warning">ปฏิเสธ</h3>
                                            <strong class="text-5 ">40</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="#">
                            <div class="card card-modern">
                                <div class="card-body py-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-4 text-left text-md-right pr-md-4 mt-4 mt-md-0">
                                            <i class="bx bx-list-check icon icon-inline icon-xl "></i>
                                        </div>
                                        <div class="col-8 col-md-8">
                                            <h3 class="text-4-1 my-0 text-success">อนุมัติ</h3>
                                            <strong class="text-5 ">1735</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-right mb-3">
                                    <button type="button" class="btn btn-export"><i class="bx bxs-file-export"></i> ดาวน์โหลด CSV</button>
                                </div>
                                <table class="table  table-striped mb-0" id="datatable-default">

                                    <thead>
                                        <tr class="head-table">
                                            <th class="center">ลำดับ</th>
                                            <th class="center">เลขที่เอกสร</th>
                                            <th class="center">วันที่</th>
                                            <th class="center">แผนก</th>
                                            <th class="center">เรื่อง</th>
                                            <th class="center">แสดงความคิดเห็น</th>
                                            <th class="center ">ไฟล์แนบ</th>
                                            <th class="center ">สถานะ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr onclick="window.location='3-0-1.php';">
                                            <td class="center ">1</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="center "><a href="#"><i class="fas fa-comment-dots"></i> 2</a></td>
                                            <td class="center "><a href="#"><i class="fas fa-paperclip"></i> 1</a></td>
                                            <td class="center "><button type="button" class="btn btn-sm btn-warning">รอการอนุมัติ</button></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>




            </section>
        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>