/* Add here all your JS customizations */

function myFunction() {
  document.getElementsByClassName("delete-row"); {
    swal({
      title: "คุณแน่ใจว่าต้องการลบใช่ไหม?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      buttons: ["ยกเลิก", "ยืนยัน"],
    }).then((willDelete) => {
      if (willDelete) {
        swal("ข้อมูลถูกลบเรียบร้อยเเล้ว", {
          icon: "success",
        });
      }
    });
  }
}

$('.modal-with-zoom-anim').magnificPopup({
  type: 'inline',

  fixedContentPos: false,
  fixedBgPos: true,

  overflowY: 'auto',

  closeBtnInside: true,
  closeOnBgClick: true,
  preloader: false,

  midClick: true,
  removalDelay: 300,
  mainClass: 'my-mfp-zoom-in',
  modal: true
});

$(document).on("click", ".modal-dismiss", function (e) {
  e.preventDefault();
  $.magnificPopup.close();
});

//hide all tabs first
// $(".select-tab").hide();
//show the first tab content
$("#tab-1-1").show();

$(".select-box-tab").change(function () {
  dropdown = $(".select-box-tab").val();
  //first hide all tabs again when a new option is selected
  $(".select-tab").hide();
  //then show the tab content of whatever option value was selected
  $("#" + "tab-" + dropdown).show();
});