<aside id="sidebar-left" class="sidebar-left">


    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">

                <ul class="nav nav-main ">
                    <li class="show">
                        <a class="nav-link" href="index.php">
                            <i class='bx bxs-news'></i>
                            <span>ข่าวใหม่</span>
                        </a>
                    </li>
                    <li class="nav-parent show">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-folder-open'></i>
                            <span>จัดการประเภทเอกสาร</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="1-1-create-document.php">
                                    เพิ่มเอกสาร
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="1-2-create-template.php">
                                    เพิ่ม Template
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="show">
                        <a class="nav-link" href="2-0-pending-approval.php">
                            <i class='bx bxs-folder-open'></i>
                            <span>รอการอนุมัติ</span>
                            <span class="float-right badge badge-danger">10</span>
                        </a>
                    </li>

                    <li class="show">
                        <a class="nav-link" href="3-0-create-document.php">
                            <i class='bx bxs-file-plus'></i>
                            <span>สร้างเอกสาร</span>
                        </a>
                    </li>

                    <li class="show">
                        <a class="nav-link" href="4-0.php">
                            <i class='bx bxs-user'></i>
                            <span>จัดการผู้ใช้งาน</span>
                        </a>
                    </li>

                    <li class="show">
                        <a class="nav-link" href="5-0.php">
                            <i class='bx bxs-user-detail'></i>
                            <span>จัดการสายการอนุมัติ</span>
                        </a>
                    </li>
                    <li class="show">
                        <a class="nav-link" href="6-0.php">
                            <i class='bx bxs-user-detail'></i>
                            <span>จัดการสายอนุมัติผู้บริหาร</span>
                        </a>
                    </li>

                    <li class="show">
                        <a class="nav-link" href="7-0.php">
                            <i class='bx bxs-wallet'></i>
                            <span>การเงิน</span>
                        </a>
                    </li>

                    <li class="show">
                        <a class="nav-link" href="8-0.php">
                            <i class='bx bxs-cog'></i>
                            <span>ตั้งค่าเว็บไซต์</span>
                        </a>
                    </li>
                </ul>




        </div>


        <script>
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>


    </div>

</aside>