  <header class="header">
      <div class="logo-container">
          <a href="index.php" class="logo">
              <img src="img/logo-dark.svg" height="50" alt="" />
          </a>
          <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
              <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
          </div>
      </div>

      <div class="header-right">

          <span class="separator"></span>

          <ul class="notifications">
              <li>
                  <a class="dropdown-language nav-link" href="#" role="button" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <img src="img/th-flag.png" class="" alt="Thailand" />
                      &nbsp;<small>ไทย
                          <i class="fas fa-chevron-down"></i>
                      </small>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
                      <a class="dropdown-item" href="#">
                          <img src="img/th-flag.png" class="" alt="Thailand" />
                          &nbsp;ไทย
                      </a>
                      <a class="dropdown-item" href="#">
                          <img src="img/en-flag.png" class="" alt="English" />
                          &nbsp;EN
                      </a>

                  </div>
              </li>
              <li>
                  <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                      <i class="bx bx-bell"></i>
                      <span class="badge">3</span>
                  </a>

                  <div class="dropdown-menu notification-menu">
                      <div class="notification-title">
                          <span class="float-right badge badge-default">3</span>
                          แจ้งเตือน
                      </div>

                      <div class="content">
                          <ul>
                              <li>
                                  <a href="#" class="clearfix">
                                      <div class="image">
                                          <i class="far fa-comment-dots bg-info text-light"></i>
                                      </div>
                                      <span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat architecto dolores, consequuntur quibusdam ad ipsam minus fugiat dolorem blanditiis numquam tempore hic. Numquam tenetur eum aspernatur autem! Officia, eligendi culpa.</span>
                                      <span class="message">Just now</span>
                                  </a>
                              </li>
                              <li>
                                  <a href="#" class="clearfix">
                                      <div class="image">
                                          <i class="far fa-comment-dots bg-info text-light"></i>
                                      </div>
                                      <span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat architecto dolores.</span>
                                      <span class="message">10/10/2017</span>
                                  </a>
                              </li>
                              <li>
                                  <a href="#" class="clearfix">
                                      <div class="image">
                                          <i class="far fa-comment-dots bg-info text-light"></i>
                                      </div>
                                      <span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat architecto dolores.</span>
                                      <span class="message">10/10/2017</span>
                                  </a>
                              </li>
                              <li>
                                  <a href="#" class="clearfix">
                                      <div class="image">
                                          <i class="far fa-comment-dots bg-info text-light"></i>
                                      </div>
                                      <span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat architecto dolores.</span>
                                      <span class="message">10/10/2017</span>
                                  </a>
                              </li>
                              <li>
                                  <a href="#" class="clearfix">
                                      <div class="image">
                                          <i class="far fa-comment-dots bg-info text-light"></i>
                                      </div>
                                      <span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat architecto dolores.</span>
                                      <span class="message">10/10/2017</span>
                                  </a>
                              </li>
                          </ul>

                          <hr />

                          <div class="text-right">
                              <a href="#" class="view-more">ดูเพิ่มเติม</a>
                          </div>
                      </div>
                  </div>
              </li>
          </ul>

          <span class="separator"></span>

          <div id="userbox" class="userbox">
              <a href="#" data-toggle="dropdown">
                  <figure class="profile-picture">
                      <img src="img/!logged-user.jpg" alt="Joseph Doe" class="rounded-circle" data-lock-picture="img/!logged-user.jpg" />
                  </figure>
                  <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                      <span class="name">Fullnanme</span>
                      <span class="role">Administrator</span>
                  </div>

                  <i class="fa custom-caret"></i>
              </a>

              <div class="dropdown-menu">
                  <ul class="list-unstyled mb-2">
                      <li>
                          <a role="menuitem" tabindex="-1" href="pages-user-profile.php"><i class="bx bx-user-circle"></i> ข้อมูลส่วนตัว</a>
                      </li>
                      <li>
                          <a role="menuitem" tabindex="-1" href="pages-signin.php"><i class="bx bx-power-off"></i> ออกจากระบบ</a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </header>