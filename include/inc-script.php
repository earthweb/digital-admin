 <!-- Vendor -->
 <script src="vendor/jquery/jquery.js"></script>
 <script src="vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
 <script src="vendor/popper/umd/popper.min.js"></script>
 <script src="vendor/bootstrap/js/bootstrap.js"></script>
 <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
 <script src="vendor/common/common.js"></script>
 <script src="vendor/nanoscroller/nanoscroller.js"></script>
 <script src="vendor/magnific-popup/jquery.magnific-popup.js"></script>
 <script src="vendor/jquery-placeholder/jquery.placeholder.js"></script>

 <!-- Specific Page Vendor -->
 <script src="vendor/jquery-ui/jquery-ui.js"></script>
 <script src="vendor/jqueryui-touch-punch/jquery.ui.touch-punch.js"></script>
 <script src="vendor/jquery-appear/jquery.appear.js"></script>
 <script src="vendor/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
 <script src="vendor/jquery.easy-pie-chart/jquery.easypiechart.js"></script>
 <script src="vendor/flot/jquery.flot.js"></script>
 <script src="vendor/flot.tooltip/jquery.flot.tooltip.js"></script>
 <script src="vendor/flot/jquery.flot.pie.js"></script>
 <script src="vendor/flot/jquery.flot.categories.js"></script>
 <script src="vendor/flot/jquery.flot.resize.js"></script>
 <script src="vendor/jquery-sparkline/jquery.sparkline.js"></script>
 <script src="vendor/raphael/raphael.js"></script>
 <script src="vendor/morris/morris.js"></script>
 <script src="vendor/gauge/gauge.js"></script>
 <script src="vendor/snap.svg/snap.svg.js"></script>
 <script src="vendor/liquid-meter/liquid.meter.js"></script>
 <script src="vendor/chartist/chartist.js"></script>
 <script src="vendor/jqvmap/jquery.vmap.js"></script>
 <script src="vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
 <script src="vendor/jqvmap/maps/jquery.vmap.world.js"></script>
 <script src="vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
 <script src="vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
 <script src="vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
 <script src="vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
 <script src="vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
 <script src="vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>

 <script src="vendor/select2/js/select2.js"></script>
 <script src="vendor/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
 <script src="vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
 <script src="vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
 <script src="vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
 <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
 <script src="vendor/fuelux/js/spinner.js"></script>
 <script src="vendor/dropzone/dropzone.js"></script>
 <script src="vendor/bootstrap-markdown/js/markdown.js"></script>
 <script src="vendor/bootstrap-markdown/js/to-markdown.js"></script>
 <script src="vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
 <script src="vendor/codemirror/lib/codemirror.js"></script>
 <script src="vendor/codemirror/addon/selection/active-line.js"></script>
 <script src="vendor/codemirror/addon/edit/matchbrackets.js"></script>
 <script src="vendor/codemirror/mode/javascript/javascript.js"></script>
 <script src="vendor/codemirror/mode/xml/xml.js"></script>
 <script src="vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
 <script src="vendor/codemirror/mode/css/css.js"></script>
 <script src="vendor/summernote/summernote-bs4.js"></script>
 <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
 <script src="vendor/ios7-switch/ios7-switch.js"></script>
 <script src="vendor/autosize/autosize.js"></script>
 <script src="vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
 <!-- <script src="vendor/jquery-validation/jquery.validate.js"></script> -->
 <script src="vendor/owl.carousel/owl.carousel.js"></script>
 <script src="vendor/codecanyon/js/slim.kickstart.min.js"></script>
 


 <script src="vendor/datatables/media/js/jquery.dataTables.min.js"></script>
 <script src="vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
 <script src="vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
 <script src="vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
 <script src="vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
 <script src="vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
 <script src="vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
 <script src="vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
 <script src="vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>
 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



 <!--(remove-empty-lines-end)-->

 <!-- Theme Base, Components and Settings -->
 <script src="js/theme.js"></script>

 <!-- Theme Custom -->
 <script src="js/custom.js"></script>

 <!-- Theme Initialization Files -->
 <script src="js/theme.init.js"></script>

 <!-- Examples -->
 <script src="js/examples/examples.dashboard.js"></script>
 <!-- <script src="js/examples/examples.validation.js"></script> -->
 <script src="js/examples/examples.advanced.form.js"></script>
 <script src="js/examples/examples.datatables.default.js"></script>
 <script src="js/examples/examples.datatables.row.with.details.js"></script>
 <script src="js/examples/examples.datatables.tabletools.js"></script>
 <script src="js/examples/examples.charts.js"></script>
