<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>สร้างเอกสาร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>จัดการเอกสาร</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>


                <div class="row">
                    <div class="col">
                        <section class="card card-modern card-big-info">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-2 col-xl-2">
                                        <i class="card-big-info-icon bx bx-file"></i>
                                        <h2 class="card-big-info-title">สร้างเอกสาร</h2>
                                        <p class="card-big-info-desc"></p>
                                    </div>

                                    <div class="col-lg-10 col-xl-10">
                                        <div class="form-group row align-items-center">
                                            <label class="col-lg-3 control-label text-lg-right pt-2 text-5">ประเภทของเอกสาร <span class="required">*</span></label>
                                            <div class="col-lg-6">
                                                <select id="" class="form-control select-box-tab">
                                                    <option value="0">ประเภทเอกสาร</option>
                                                    <option value="1-1">ราคากลาง</option>
                                                    <option value="2-2">บัญชีรายการพัสดุที่ต้องการซื้อ/จ้าง</option>
                                                    <option value="3-3">เอกสารข้อกำหนดคุณลักษณะพัสดุที่ต้องการซื้อ
                                                        <!--  / จ้าง สำหรับวิธี eBidding วิธีคัดเลือก และวิธีเฉพาะเจาะจงกรณีวงเงินเกิน 5 แสนบาท -->
                                                    </option>
                                                    <option value="4-4" selected>ใบคำขอการจัดซื้อ / จัดจ้าง</option>
                                                    <option value="5-5">ใบเงินยืมทดรอง</option>
                                                    <option value="6-6">ใบเคลียร์เงินทดรองจ่าย</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div id="tab-4-4" class="select-tab ">
                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">1</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">เลขจองงบประมาณ</label>
                                                    <input type="text" class="form-control">
                                                    <small class="notice">กรุณากดปุ่ม Enter เมื่อกรอกข้อมูลเลขจองงบประมาณเรียบร้อยแล้ว</small>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3 offset-lg-1">
                                                    <label class="title-label">เลขจองงบประมาณ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <label class="title-label">จำนวนเงิน</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <label class="title-label">เลขที่โครงการ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3 offset-lg-1">
                                                    <label class="title-label">ชื่อโครงการ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <label class="title-label">ฝ่ายเจ้าของโครงการ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                                <div class="col-lg-3 ">
                                                    <label class="title-label">แหล่งเงิน</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-3 offset-lg-1">
                                                    <label class="title-label">ปีงบประมาณ</label>
                                                    <input type="text" disabled class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">2</span></div>
                                                <div class="col-lg-3 col-xl-3">
                                                    <label class="title-label">แผนก</label>
                                                    <input type="text" class="form-control" disabled value="ฝ่ายบริหารกลาง">
                                                </div>
                                                <div class="col-lg-3 col-xl-3">
                                                    <label class="title-label">กลุ่ม</label>
                                                    <input type="text" class="form-control" disabled value="">
                                                </div>
                                                <div class="col-lg-3 col-xl-3">
                                                    <label class="title-label">
                                                        งาน/ห้องปฏิบัติการ</label>
                                                    <input type="text" class="form-control" disabled value="">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">3</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">วิธีการจัดซื้อ / จัดจ้าง <span class="required">*</span></label>
                                                    <div class="radio-custom radio-primary">
                                                        <input type="radio" id="radioExample1" name="radioExample">
                                                        <label for="radioExample1">วิธี e-bidding</label>
                                                    </div>
                                                    <div class="radio-custom radio-primary">
                                                        <input type="radio" id="radioExample2" name="radioExample">
                                                        <label for="radioExample2">วิธีคัดเลือก</label>
                                                    </div>
                                                    <div class="radio-custom radio-primary">
                                                        <input type="radio" id="radioExample3" name="radioExample">
                                                        <label for="radioExample3">วิธีเฉพาะเจาะจง</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">4</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">พัสดุที่ต้องการจัดหา</label>
                                                    <input type="text" class="form-control" placeholder="พัสดุที่ต้องการจัดหา">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">5</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">ประเภทการจัดซื้อ <span class="required">*</span></label>
                                                    <div class="radio-custom radio-primary">
                                                        <input type="radio" id="radioExample1" name="radioExample">
                                                        <label for="radioExample1">ซื้อ</label>
                                                    </div>
                                                    <div class="radio-custom radio-primary">
                                                        <input type="radio" id="radioExample2" name="radioExample">
                                                        <label for="radioExample2">จัดจ้าง</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">6</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">ระเบียบพัสดุที่ซื้อ <span class="required">*</span></label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" selected>ระเบียบพัสดุที่ซื้อ</option>
                                                        <option value="">test</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">7</span></div>
                                                <div class="col-lg-7 col-xl-11">
                                                    <label class="title-label">เหตุผลและความจำเป็น <span class="required">*</span></label>
                                                    <textarea name="content" data-plugin-markdown-editor rows="10">Start typing...</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">8</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">เอกสารแนบ 1: คุณลักษณะพัสดุที่ต้องการจัดหา <span class="required">*</span></label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" disabled selected></option>
                                                        <option value="">test</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row bt-0 pt-0">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" disabled selected></option>
                                                        <option value="">test</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">9</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">เอกสารแนบ 2: ราคากลาง <span class="required">*</span></label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" disabled selected></option>
                                                        <option value="">test</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row bt-0">
                                                <div class="col-lg-1"><span class="badge-title badge ">10</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">วงเงินงบประมาณ </label>
                                                    <div class="input-group">
                                                        <input type="number" class="form-control" disabled value="" placeholder="วงเงินงบประมาณ">
                                                        <span class="input-group-append">
                                                            <span class="input-group-text">
                                                                บาท
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">11</span></div>
                                                <div class="col-lg-7 col-xl-8">
                                                    <label class="title-label">วิธีที่จะซื้อหรือจ้างและเหตุผลที่ต้องซื้อหรือจ้างโดยวิธีนั้น </label>
                                                    <textarea class="form-control form-control-modern" name="purchaseNote" rows="6" disabled></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">12</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">หลักเกณฑ์การพิจารณาข้อเสนอ</label>
                                                    <input type="text" class="form-control" disabled placeholder="ปรากฎตามเอกสารแนบที่ 1">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">13</span></div>
                                                <div class="col-lg-7 col-xl-11">
                                                    <label class="title-label">อื่นๆ </label>
                                                    <textarea name="content" data-plugin-markdown-editor rows="10">Start typing...</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row align-items-center">
                                                <div class="col-lg-1"><span class="badge-title badge ">14</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">เอกสารแนบที่ 3 :</label>
                                                    <form action="/upload" class="dropzone dz-square dz-clickable" id="dropzone-example">
                                                        <div class="dz-default dz-message"><button class="dz-button" type="button">Drop files here to upload</button></div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="form-group row align-items-center">
                                                <div class="col-lg-1"><span class="badge-title badge ">15</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">เอกสารแนบที่ 4 :</label>
                                                    <form action="/upload" class="dropzone dz-square dz-clickable" id="dropzone-example">
                                                        <div class="dz-default dz-message"><button class="dz-button" type="button">Drop files here to upload</button></div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="form-group row align-items-center">
                                                <div class="col-lg-1"><span class="badge-title badge ">16</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">เอกสารแนบที่ 5 :</label>
                                                    <input type="text" class="form-control mt-3 mb-3" placeholder="">
                                                    <form action="/upload" class="dropzone dz-square dz-clickable" id="dropzone-example">
                                                        <div class="dz-default dz-message"><button class="dz-button" type="button">Drop files here to upload</button></div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">17</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">จัดการสายอนุมัติ <span class="required">*</span></label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" selected>เลือกรายการสายอนุมัติ</option>
                                                        <option value="">test</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-lg-1"><span class="badge-title badge ">18</span></div>
                                                <div class="col-lg-7 col-xl-6">
                                                    <label class="title-label">จัดการสายการอนุมัติ ผู้บริหาร <span class="required">*</span></label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" selected>เลือกรายการอนุมัติ ผู้บริหาร</option>
                                                        <option value="">test</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                            </div>
                    </div>
            </section>
        </div>
        </div>


        <div class="action-buttons-fixed">
            <div class="row action-buttons">
                <div class="col-12 col-md-auto">
                    <button type="submit" class="submit-button btn btn-primary btn-px-4 py-3 d-flex align-items-center font-weight-semibold line-height-1" data-loading-text="Loading...">
                        <i class="bx bx-save text-4 mr-2"></i> บันทึกข้อมูล
                    </button>
                </div>
                <div class="col-12 col-md-auto px-md-0 mt-3 mt-md-0">
                    <a href="#" class="cancel-button btn btn-light btn-px-4 py-3 border font-weight-semibold text-color-dark text-3"><i class="far fa-eye"></i> ดูตัวอย่าง</a>
                </div>
            </div>
        </div>
    </section>

    </div>


    </section>
    <?php include 'include/inc-script.php'; ?>



</body>

</html>