<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดการผู้ใช้งาน</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>



                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <h5>ชื่อ-นามสกุล</h5>
                                            <div class="">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 mb-4 mb-xl-0">
                                        <section class="card">
                                            <div class="card-body">
                                                <div class="thumb-info mb-3">
                                                    <img src="img/!logged-user.jpg" class="rounded img-fluid" alt="John Doe">
                                                    <div class="thumb-info-title">
                                                        <span class="thumb-info-inner">ชื่อ-นามสกุล</span>
                                                        <span class="thumb-info-type">ตำแหน่ง</span>
                                                    </div>
                                                </div>

                                                <div class="widget-toggle-expand mb-3">
                                                    <div class="widget-content-expanded">
                                                        <ul class="simple-todo-list mt-3">
                                                            <li class="completed">Update Profile Picture</li>
                                                            <li class="completed">Change Personal Information</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-lg-8 col-xl-8">

                                        <div class="tabs">
                                            <ul class="nav nav-tabs tabs-primary">
                                                <li class="nav-item active">
                                                    <a class="nav-link" href="#overview" data-toggle="tab">ข้อมูลส่วนตัว</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#edit" data-toggle="tab">แก้ไข</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="overview" class="tab-pane active">

                                                    <div class="p-3">
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                ชื่อผู้ใช้งาน</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0">Username</p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                ชื่อ</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                นามสกุล</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                ตำแหน่งงาน</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                แผนก</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                เบอร์โทรศัพท์</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                อีเมล์</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                ลายเซ็น</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0">
                                                                    <span class="text-danger">ยังไม่ได้อัพโหลดลายเซ็น</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div id="edit" class="tab-pane">

                                                    <form class="p-3">
                                                        <h4 class="mb-3">ข้อมูลเบื้องต้น</h4>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                ชื่อ</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                นามสกุล</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                ตำแหน่งงาน</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                แผนก</label>
                                                            <div class="col-xl-8">
                                                                <select id="tags-input-multiple" data-plugin-selectTwo class="form-control populate" multiple>
                                                                    <option value="1">แผนก 1</option>
                                                                    <option value="2">แผนก 2</option>
                                                                    <option value="3">แผนก 3</option>
                                                                    <option value="4">แผนก 4</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                เบอร์โทรศัพท์</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                อีเมล์</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>

                                                        <hr class="dotted tall">

                                                        <h4 class="mb-3">เปลื่ยนรหัสผ่าน</h4>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                รหัสผ่านใหม่</label>
                                                            <div class="col-xl-8">
                                                                <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                ยืนยันหัสผ่านใหม่</label>
                                                            <div class="col-xl-8">
                                                                <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-12 text-right mt-3">
                                                                <button class="btn btn-primary modal-confirm">บันทึก</button>
                                                            </div>
                                                        </div>

                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>




            </section>
        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>

</body>

</html>