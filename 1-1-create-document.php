<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มเอกสาร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>จัดการประเภทเอกสาร</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>
                <div class="row">
                    <div class="col">
                        <section class="card">
                            <header class="card-header">
                                <h2 class="card-title">จัดการประเภทเอกสาร สำหรับเอกสารประเภท Upload</h2>
                            </header>
                            <div class="card-body">
                                <a type="button" href="#modalAnim" class="modal-with-zoom-anim mb-1 mt-1 mr-1 btn btn-primary btn-px-4 py-3"><i class="fas fa-plus mr-2"></i> เพิ่มประเภทเอกสาร</a>
                                <div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
                                    <section class="card">
                                        <header class="card-header">
                                            <h2 class="card-title">เพิ่มประเภทเอกสาร</h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="form-group ">
                                                <label for="">ประเภทเอกสาร (ภาษาไทย)<span class="required">*</span></label>
                                                <input type="text" class="form-control" id="" placeholder="">
                                            </div>
                                            <div class="form-group ">
                                                <label for="">ประเภทเอกสาร (อังกฤษ)<span class="required">*</span></label>
                                                <input type="text" class="form-control" id="" placeholder="">
                                            </div>
                                            <div class="form-group ">
                                                <label for="">รหัสเอกสาร<span class="required">*</span></label>
                                                <input type="text" class="form-control" id="" placeholder="">
                                            </div>

                                            <div class="form-group ">
                                                <label for="">อัพโหลด</label>
                                                <form action="/upload" class="dropzone dz-square" id="dropzone-example"></form>
                                                <div class="mt-1">
                                                    <i class="fas fa-info-circle text-main"></i> ตัวอย่างไฟล์ : <strong class="text-main">import_upload_memo_type_example.xlsx </strong>
                                                </div>
                                            </div>

                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button class="btn btn-primary w-150">ตกลง</button>
                                                    <button class="btn btn-default modal-dismiss w-150">ยกเลิก</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </div>
                                <hr>
                                <table class="table  table-striped mb-0" id="datatable-default">

                                    <thead>
                                        <tr class="head-table">
                                            <th class="center  ">ลำดับ</th>
                                            <th>ประเภทเอกสาร</th>
                                            <th class="center ">รหัสเอกสาร</th>
                                            <th class="center ">แก้ไข</th>
                                            <th class="center ">ลบ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="center ">1</td>
                                            <td>
                                            </td>
                                            <td class="center "></td>
                                            <td class="actions center">
                                                <a href="#" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                            </td>
                                            <td class="actions center">
                                                <a href="#" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center ">2</td>
                                            <td>
                                            </td>
                                            <td class="center "></td>
                                            <td class="actions center">
                                                <a href="#" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                            </td>
                                            <td class="actions center">
                                                <a href="#" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="center ">3</td>
                                            <td>
                                            </td>
                                            <td class="center "></td>
                                            <td class="actions center">
                                                <a href="#" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                            </td>
                                            <td class="actions center">
                                                <a href="#" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </section>

                    </div>
                </div>

            </section>

        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>