<!doctype html>
<html class="fixed">

<head>
    <title>เข้าสู่ระบบ</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body-sign">
        <div class="center-sign">
            <a href="#" class="logo float-left">
                <img src="img/logo-dark.svg" height="54" alt="Porto Admin" />
            </a>

            <div class="panel card-sign">
                <div class="card-title-sign mt-3 text-right">
                    <h2 class="title text-uppercase font-weight-bold m-0"><i class="bx bx-user-circle mr-1 text-6 position-relative top-5"></i> Sign In</h2>
                </div>
                <div class="card-body">
                    <form action="" method="post">

                        <div class="form-group mb-3">
                            <label>บัญชีผู้ใช้งาน</label>
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="bx bx-user text-4"></i>
                                    </span>
                                </span>
                                <input type="text" class="form-control form-control-lg" placeholder="">
                            </div>
                        </div>



                        <div class="form-group mb-3">
                            <div class="clearfix">
                                <label class="float-left">รหัสผ่าน</label>
                                <a href="pages-recover-password.php" class="float-right">ลืมรหัสผ่าน ?</a>
                            </div>
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="bx bx-lock text-4"></i>
                                    </span>
                                </span>
                                <input type="password" class="form-control form-control-lg" placeholder="">
                            </div>
                        </div>


                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <div class="checkbox-custom checkbox-default">
                                    <input id="RememberMe" name="rememberme" type="checkbox" />
                                    <label for="RememberMe">จดจำฉัน</label>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12 ">
                                <a href="index.php" class="btn btn-primary p-2 mt-2 w-100">เข้าสู่ระบบ</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <p class="text-center text-muted mt-3 mb-3">&copy; Copyright 2022. All Rights Reserved.</p>
        </div>
    </section>


    <?php include 'include/inc-script.php'; ?>
</body>

</html>