<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดการผู้ใช้งาน</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>



                <div class="row">
                    <div class="col-lg-3">
                        <section class="card">
                            <header class="card-header">
                                <h2 class="card-title d-flex align-items-center justify-content-between">จัดการผู้ใช้งาน <button href="#modalAnim" class="modal-with-zoom-anim btn btn-ol-white btn-outline-light  text-white"><i class="fas fa-user-plus"></i> เพิ่ม</button></h2>
                                <div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-lg modal-block-primary mfp-hide">
                                    <section class="card">
                                        <header class="card-header">
                                            <h2 class="card-title">สร้างผู้ใช้งานใหม่</h2>
                                        </header>
                                        <div class="card-body">
                                            <form class="p-3">
                                                <div class="row">
                                                    <div class="col-lg-6 mb-2">
                                                        <label class="title-label">
                                                            อีเมล์</label>
                                                        <div class="">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 mb-2">
                                                        <label class="title-label">
                                                            รหัสผ่าน</label>
                                                        <div class="">
                                                            <input type="password" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 mb-2">
                                                        <label class="title-label">
                                                            ชื่อ</label>
                                                        <div class="">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 mb-2">
                                                        <label class="title-label">
                                                            นามสกุล</label>
                                                        <div class="">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 mb-2">
                                                        <label class="title-label">
                                                            แผนก</label>
                                                        <div class="">
                                                            <select id="" data-plugin-selectTwo class="form-control populate" multiple>
                                                                <option value="1">แผนก 1</option>
                                                                <option value="2">แผนก 2</option>
                                                                <option value="3">แผนก 3</option>
                                                                <option value="4">แผนก 4</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 mb-2">
                                                        <label class="title-label">
                                                            บทบาท</label>
                                                        <div class="">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <option value="">บทบาท</option>
                                                                <option value="">1</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 mb-2">
                                                        <label class="title-label">
                                                            ตำแหน่งงาน</label>
                                                        <div class="">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="form-group col">
                                                        <label class="title-label" for="">อัพโหลด</label>
                                                        <form action="/upload" class="dropzone dz-square" id="dropzone-example"></form>
                                                        <form action="/upload" class="dropzone dz-square" id="dropzone-example"></form>
                                                        <div class="mt-1">
                                                            <i class="fas fa-info-circle text-main"></i> ตัวอย่างไฟล์ : <strong class="text-main">add_user_example.xlsx </strong>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>


                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button class="btn btn-primary w-150">ตกลง</button>
                                                    <button class="btn btn-default modal-dismiss w-150">ยกเลิก</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </div>
                            </header>
                            <div class="">
                                <div class="bg-light rounded">
                                    <div class="card card-modern">
                                        <div class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                            </div>
                                            <h4 class="card-title text-dark">ค้นหารายชื่อ <span class="badge rounded-pill bg-secondary text-white">10 คน</span></h4>

                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12 mb-2">
                                                    <label class="title-label">ค้นหา</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                                <div class="col-lg-12 mb-2">
                                                    <label class="title-label">บทบาท</label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" disabled selected>บทบาท</option>
                                                        <option value="">1</option>
                                                        <option value="">2</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-12 mb-2">
                                                    <label class="title-label">ตำแหน่งงาน</label>
                                                    <select data-plugin-selectTwo class="form-control populate">
                                                        <option value="0" disabled selected>ตำแหน่งงาน</option>
                                                        <option value="">1</option>
                                                        <option value="">2</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="solid opacity-7">
                                </div>
                            </div>
                            <div class="bg-white rounded  p-0 pt-2">
                                <div class="scrollable scroll-approval" data-plugin-scrollable>
                                    <div class="scrollable-content">
                                        <ul class="list-approval">
                                            <?php
                                            for ($x = 0; $x <= 10; $x++) { ?>
                                                <li class="user-list">
                                                    <h5 class="mb-0">ชื่อ นามสกุล</h5>
                                                    <div class="position-list">
                                                        <h6>ตำแหน่ง</h6>
                                                        <div class="text-right"><i class="text-muted fas fa-arrow-right"></i></div>
                                                        <div>
                                                </li>
                                            <?php
                                            }
                                            ?>

                                        </ul>
                                    </div>
                                </div>
                        </section>
                    </div>

                    <div class="col-lg-9">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <h5>ชื่อ-นามสกุล</h5>
                                            <div class="">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 mb-4 mb-xl-0">
                                        <section class="card">
                                            <div class="card-body">
                                                <div class="thumb-info mb-3">
                                                    <img src="img/!logged-user.jpg" class="rounded img-fluid" alt="John Doe">
                                                    <div class="thumb-info-title">
                                                        <span class="thumb-info-inner">ชื่อ-นามสกุล</span>
                                                        <span class="thumb-info-type">ตำแหน่ง</span>
                                                    </div>
                                                </div>

                                                <div class="widget-toggle-expand mb-3">
                                                    <div class="widget-content-expanded">
                                                        <ul class="simple-todo-list mt-3">
                                                            <li class="completed">Update Profile Picture</li>
                                                            <li class="completed">Change Personal Information</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-lg-8 col-xl-8">

                                        <div class="tabs">
                                            <ul class="nav nav-tabs tabs-primary">
                                                <li class="nav-item active">
                                                    <a class="nav-link" href="#overview" data-toggle="tab">ข้อมูลส่วนตัว</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#edit" data-toggle="tab">แก้ไข</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="overview" class="tab-pane active">

                                                    <div class="p-3">
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                ชื่อผู้ใช้งาน</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0">Username</p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                ชื่อ</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                นามสกุล</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                ตำแหน่งงาน</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                บทบาท</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                แผนก</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                การเข้าถึงเอกสาร</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                เบอร์โทรศัพท์</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                อีเมล์</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0"></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">
                                                                ลายเซ็น</label>
                                                            <div class="col-lg-7 col-xl-6">
                                                                <p class="form-control-static mb-0">
                                                                    <span class="text-danger">ยังไม่ได้อัพโหลดลายเซ็น</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div id="edit" class="tab-pane">

                                                    <form class="p-3">
                                                        <h4 class="mb-3">ข้อมูลเบื้องต้น</h4>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                ชื่อ</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                นามสกุล</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                ตำแหน่งงาน</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                บทบาท</label>
                                                            <div class="col-xl-8">
                                                                <select data-plugin-selectTwo class="form-control populate">
                                                                    <option value="">บทบาท</option>
                                                                    <option value="">1</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                แผนก</label>
                                                            <div class="col-xl-8">
                                                                <select id="tags-input-multiple" data-plugin-selectTwo class="form-control populate" multiple>
                                                                    <option value="1">แผนก 1</option>
                                                                    <option value="2">แผนก 2</option>
                                                                    <option value="3">แผนก 3</option>
                                                                    <option value="4">แผนก 4</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                การเข้าถึงเอกสาร</label>
                                                            <div class="col-xl-8">
                                                                <select data-plugin-selectTwo class="form-control populate">
                                                                    <option value="">การเข้าถึงเอกสาร</option>
                                                                    <option value="">1</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                เบอร์โทรศัพท์</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                อีเมล์</label>
                                                            <div class="col-xl-8">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center">
                                                            <label class="col-xl-3 control-label text-lg-right mb-0">
                                                                ลายเซ็น</label>
                                                            <div class="col-xl-8">
                                                                <form action="/upload" class="dropzone dz-square dz-clickable" id="dropzone-example" style="display:none">
                                                                    <div class="dz-default dz-message" style="display:none"><button class="dz-button" type="button">Drop files here to upload</button></div>
                                                                </form>
                                                                <form action="/upload" class="dropzone dz-square dz-clickable" id="dropzone-example">
                                                                    <div class="dz-default dz-message"><button class="dz-button" type="button">Drop files here to upload</button></div>
                                                                </form>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-12 text-right mt-3">
                                                                <button class="btn btn-primary modal-confirm">บันทึก</button>
                                                            </div>
                                                        </div>

                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>




            </section>
        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>

</body>

</html>