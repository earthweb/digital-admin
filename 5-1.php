<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>สร้างสายการอนุมัติใหม่</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>จัดการสายการอนุมัติ</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>


                <div class="row">
                    <div class="col">
                        <section class="card card-modern card-big-info">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-2 col-xl-2">
                                        <i class="card-big-info-icon bx bx-file"></i>
                                        <h2 class="card-big-info-title">สร้างสายการอนุมัติใหม่</h2>
                                        <p class="card-big-info-desc"></p>
                                    </div>
                                    <div class="col-lg-10 col-xl-10">
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-8 col-xl-6">
                                                <label class="title-label">ชื่อเรียกสายการอนุมัติ</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-8 col-xl-6">
                                                <label class="title-label">เลือกแผนก</label>
                                                <select id="" data-plugin-selectTwo class="form-control populate" multiple>
                                                    <option value="1">แผนก 1</option>
                                                    <option value="2">แผนก 2</option>
                                                    <option value="3">แผนก 3</option>
                                                    <option value="4">แผนก 4</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-8 col-xl-6">
                                                <label class="title-label">สายการอนุมัติ </label>
                                                <button type="button" class=" mb-1 mr-1 btn btn-primary btn-px-4 py-3 w-100"><i class="fas fa-plus mr-2"></i> เพิ่มระดับการอนุมัติ</button>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="add-approve">
                                                    <hr class="dotted short">
                                                    <div class="row">
                                                        <div class="form-group col mb-3">
                                                            <h5><strong>ระดับการอนุมัติ #1</strong>
                                                                <div class="checkbox-custom checkbox-default d-inline-block">
                                                                    <input type="checkbox" checked="" id="checkboxExample2">
                                                                    <label for="checkboxExample2"><small>ต้องการลายเซ็นเพื่อทำการอนุมัติ</small></label>
                                                                </div>
                                                            </h5>
                                                            <div class="date-time-field justify-content-between">
                                                                <div class="time">
                                                                    <input type="number" class="form-control form-control-modern text-center" name="orderTimeHour" value="1" required />
                                                                    <span class="px-2">of</span>
                                                                    <input type="number" class="form-control form-control-modern text-center" disabled name="orderTimeHour" value="1" required />
                                                                    <select class="mx-2 select-box-tab form-control form-control-modern" name="">
                                                                        <option value="" selected>เลือกประเภท</option>
                                                                        <option value="user">user</option>
                                                                        <option value="non-user">non-user</option>
                                                                    </select>
                                                                </div>
                                                                <button type="button" class="mb-1 mr-1 btn btn-dark btn-px-2 py-2 "><i class="fas fa-plus mr-2"></i> เพิ่มผู้อนุมัติ</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="tab-user" class="select-tab mt-3">
                                                        <div class="row">
                                                            <div class="col-lg-6 item-add">
                                                                <div class="row">
                                                                    <div class="col-lg-10">
                                                                        <div class="form-group row">
                                                                            <div class="col-lg-12">
                                                                                <label class="title-label ">Free text(optional)</label>
                                                                                <input type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <div class="col-lg-12">
                                                                                <label class="title-label ">เลือกการอนุมัติ</label>
                                                                                <select id="" data-plugin-selectTwo class="form-control populate">
                                                                                    <option value="1">ชื่อ นามสกุล</option>
                                                                                    <option value="2">ชื่อ นามสกุล</option>
                                                                                    <option value="3">ชื่อ นามสกุล</option>
                                                                                    <option value="4">ชื่อ นามสกุล</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <button type="button" disabled="" class="mb-1 btn-remove-user mr-1 btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="tab-non-user" class="select-tab mt-3">
                                                        <div class="row">
                                                            <div class="col-lg-6 item-add">
                                                                <div class="row">
                                                                    <div class="col-lg-10">
                                                                        <div class="form-group row">
                                                                            <div class="col-lg-12">
                                                                                <label class="title-label ">Free text(optional)</label>
                                                                                <input type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <div class="col-lg-12">
                                                                                <label class="title-label ">อีเมล์</label>
                                                                                <input type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <div class="col-lg-12">
                                                                                <label class="title-label ">ชื่อ นามสกุล</label>
                                                                                <input type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <div class="col-lg-12">
                                                                                <label class="title-label ">ตำแหน่งงาน</label>
                                                                                <input type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <button type="button" disabled="" class="mb-1 btn-remove-user mr-1 btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-8 col-xl-6">
                                                <label class="title-label">อัพโหลดไฟล์แนบ</label>
                                                <form action="/upload" class="dropzone dz-square" id="dropzone-example"></form>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>


                <div class="action-buttons-fixed">
                    <div class="row action-buttons">
                        <div class="col-12 col-md-auto">
                            <button type="submit" class="submit-button btn btn-primary btn-px-4 py-3 d-flex align-items-center font-weight-semibold line-height-1" data-loading-text="Loading...">
                                <i class="bx bx-save text-4 mr-2"></i> บันทึกข้อมูล
                            </button>
                        </div>
                        <div class="col-12 col-md-auto px-md-0 mt-3 mt-md-0">
                            <a href="#" class="cancel-button btn btn-light btn-px-4 py-3 border font-weight-semibold text-color-dark text-3">ยกเลิก</a>
                        </div>
                    </div>
                </div>
            </section>

        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>



</body>

</html>