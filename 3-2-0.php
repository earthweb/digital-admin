<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>สร้างเอกสาร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>จัดการเอกสาร</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>


                <div class="row">
                    <div class="col">
                        <section class="card card-modern card-big-info">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-2 col-xl-2">
                                        <i class="card-big-info-icon bx bx-file"></i>
                                        <h2 class="card-big-info-title">สร้างเอกสาร</h2>
                                        <p class="card-big-info-desc"></p>
                                    </div>

                                    <div class="col-lg-10 col-xl-10">
                                        <div class="form-group row align-items-center">
                                            <label class="col-lg-3 control-label text-lg-right pt-2 text-5">ประเภทของเอกสาร <span class="required">*</span></label>
                                            <div class="col-lg-6">
                                                <select id="" onchange="location = this.value;" class="form-control">
                                                    <option value="3-2-0.php">ประเภทเอกสาร</option>
                                                    <option value="3-2-1.php">ราคากลาง</option>
                                                    <option value="3-2-2.php">บัญชีรายการพัสดุที่ต้องการซื้อ/จ้าง</option>
                                                    <option value="3-2-3.php">เอกสารข้อกำหนดคุณลักษณะพัสดุที่ต้องการซื้อ
                                                        <!--  / จ้าง สำหรับวิธี eBidding วิธีคัดเลือก และวิธีเฉพาะเจาะจงกรณีวงเงินเกิน 5 แสนบาท -->
                                                    </option>
                                                    <option value="3-2-4.php">ใบคำขอการจัดซื้อ / จัดจ้าง</option>
                                                    <option value="3-2-5.php">ใบเงินยืมทดรอง</option>
                                                    <option value="3-2-5.php">ใบเคลียร์เงินทดรองจ่าย</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                      
                                    </div>




                                </div>
                            </div>
                    </div>
            </section>
        </div>
        </div>


        <div class="action-buttons-fixed">
            <div class="row action-buttons">
                <div class="col-12 col-md-auto">
                    <button type="submit" class="submit-button btn btn-primary btn-px-4 py-3 d-flex align-items-center font-weight-semibold line-height-1" data-loading-text="Loading...">
                        <i class="bx bx-save text-4 mr-2"></i> บันทึกข้อมูล
                    </button>
                </div>
                <div class="col-12 col-md-auto px-md-0 mt-3 mt-md-0">
                    <a href="#" class="cancel-button btn btn-light btn-px-4 py-3 border font-weight-semibold text-color-dark text-3"><i class="far fa-eye"></i> ดูตัวอย่าง</a>
                </div>
            </div>
        </div>
    </section>

    </div>


    </section>
    <?php include 'include/inc-script.php'; ?>



</body>

</html>