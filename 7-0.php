<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>การเงิน</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col">
                        <section class="card">
                            <header class="card-header">
                                <h2 class="card-title">การเงิน</h2>
                            </header>
                            <div class="card-body">
                                <div class="fliter-group row">
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-3 mb-2">
                                                <label class="title-label">วันที่</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="col-lg-3 mb-2">
                                                <label class="title-label">ประเภทเอกสาร</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="col-lg-3 mb-2">
                                                <label class="title-label">ชื่อเรื่อง</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 mb-2">
                                                <label class="title-label">เลขที่หนังสือ</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="col-lg-3 mb-2">
                                                <label class="title-label">สถานะ</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="col-lg-3 mb-2">
                                                <label class="title-label">เจ้าของเรื่อง</label>
                                                <input type="text" class="form-control">
                                            </div>

                                        </div>


                                    </div>


                                </div>
                                <hr>
                                <table class="table  table-striped mb-0" id="datatable-default">
                                    <thead>
                                        <tr class="head-table">
                                            <th class="center" width="10%">ลำดับ</th>
                                            <th>วันที่เข้า</th>
                                            <th>เลขที่หนังสือ</th>
                                            <th>ชื่อเรื่อง</th>
                                            <th>เจ้าของเรื่อง</th>
                                            <th>ประเภทเอกสาร</th>
                                            <th>สถานะ</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="center ">1</td>
                                            <td>2/02/2565</td>
                                            <td>อว 6101/546</td>
                                            <td>ขออนุมัติจัดซื้อหมึกพิมพ์</td>
                                            <td>สำนักผู้บริหาร</td>
                                            <td>ขอดำเนินการจัดหา</td>
                                            <td>
                                                <font color="#FFCC00">รออนุมัติ</font>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="center ">2</td>
                                            <td>2/02/2565</td>
                                            <td>อว 6101/345</td>
                                            <td>ขออนุมัติข้อจำหนดขอบเขตงานจัดซื้อคอมพิวเตอร์ 50 ชุด</td>
                                            <td>ตรวจสอบภายใน</td>
                                            <td>ข้อกำหนดขอบเขตงาน</td>
                                            <td>
                                                <font color="#33CC33">อนุมัติแล้ว</font>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="center ">3</td>
                                            <td>29/01/2565</td>
                                            <td>อว 6104/234</td>
                                            <td>ใบตรวจรับพัสดุหมึกพิมพ์ สัญญาเลขที่ 50/2565</td>
                                            <td>ฝ่ายสารสนเทศ</td>
                                            <td>ใบตรวจรับพัสดุ</td>
                                            <td><i class='bx bx-check'>เบิกจ่าย</i></td>


                                        </tr>

                                        <tr>
                                            <td class="center ">3</td>
                                            <td>28/01/2565</td>
                                            <td>อว 6106/100</td>
                                            <td>ขออนุมัติแผนจัดซือปี 2565</td>
                                            <td>ฝ่ายวิเทศสัมพันธ์</td>
                                            <td>จัดทำแผนจัดซื้อ</td>
                                            <td><i class='bx bx-x' style='color:#f70b0b'>ยกเลิก</i></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </section>

                    </div>
                </div>





            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>