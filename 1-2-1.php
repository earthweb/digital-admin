<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>

</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่ม Template เอกสาร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>จัดการประเภทเอกสาร</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <!-- <div class="row ">
                    <div class="col">
                        <section class="card">
                            <div class="card-body ">
                                <div class="form-group row align-items-center pb-3 ">
                                    <a href="1-2-create-template.php" class="mx-4"><i class="fas fa-chevron-left"></i> Template สำหรับเอกสารอัปโหลด</a>
                                </div>
                                <div class="form-group row align-items-center pb-3 ">

                                    <div class="col-lg-1"></div>
                                    <span class="badge-title badge">1</span></a>
                                    <div class="col-lg-7 col-xl-6">
                                        <label>
                                            ประเภทเอกสาร
                                        </label><br>
                                        <select class="form-control form-control-modern" name="stockStatus">
                                            <option value="percentage" selected>ประเภทเอกสาร</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row align-items-center pb-3 ">

                                    <div class="col-lg-1"></div>
                                    <span class="badge-title badge">2</span></a>
                                    <div class="col-lg-6">
                                        <label>
                                            ชื่อ
                                        </label>
                                        <input type="text" class="form-control" id="" placeholder="ชื่อ">
                                    </div>
                                </div>


                                <div class="form-group row align-items-center pb-3 ">

                                    <div class="col-lg-1"></div>
                                    <span class="badge-title badge">3</span></a>
                                    <div class="col-lg-7 col-xl-6">
                                        <label>
                                            จัดการสายอนุมัติ
                                        </label><br>
                                        <select class="form-control form-control-modern" name="stockStatus">
                                            <option value="percentage" selected>เลือกสายการอนุมัติ</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row align-items-center pb-3 ">
                                    <div class="col-lg-1"></div>
                                    <span class="badge-title badge">4</span></a>
                                    <div class="col-lg-10 ">
                                        <label>
                                            อัพโหลด pdf
                                        </label><br>
                                        <form action="/upload" class="dropzone dz-square" id="dropzone-example"></form><br>
                                        <label class="notice">
                                            *เฉพาะอัพโหลดเอกสาร(PDF ขนาด A4)
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div> -->

                <div class="row">
                    <div class="col">
                        <section class="card card-modern card-big-info">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-2-5 col-xl-1-5">
                                        <i class="card-big-info-icon bx bx-file"></i>
                                        <h2 class="card-big-info-title">สร้าง Template เอกสาร</h2>
                                        <p class="card-big-info-desc"></p>
                                    </div>
                                    <div class="col-lg-3-5 col-xl-4-5">
                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">1</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">ประเภทเอกสาร</label>
                                                <select class="form-control form-control-modern" name="stockStatus">
                                                    <option value="percentage" selected>ประเภทเอกสาร</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">2</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">ชื่อ</label>
                                                <input type="text" class="form-control form-control-modern">
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <div class="col-lg-1"><span class="badge-title badge ">3</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">จัดการสายอนุมัติ</label>
                                                <select class="form-control form-control-modern" name="stockStatus">
                                                    <option value="percentage" selected>เลือกสายการอนุมัติ</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row align-items-center">
                                        <div class="col-lg-1"><span class="badge-title badge ">4</span></div>
                                            <div class="col-lg-7 col-xl-6">
                                                <label class="title-label">อัปโหลด PDF</label>
                                                <form action="/upload" class="dropzone dz-square" id="dropzone-example"></form>
                                                <div class="notice  mt-2">
                                                    *เฉพาะอัพโหลดเอกสาร(PDF ขนาด A4)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>



                <div class="action-buttons-fixed">
                    <div class="row action-buttons">
                        <div class="col-12 col-md-auto">
                            <button type="submit" class="submit-button btn btn-primary btn-px-4 py-3 d-flex align-items-center font-weight-semibold line-height-1" data-loading-text="Loading...">
                                <i class="bx bx-save text-4 mr-2"></i> บันทึกข้อมูล
                            </button>
                        </div>
                        <div class="col-12 col-md-auto px-md-0 mt-3 mt-md-0">
                            <a href="1-2-create-template.php" class="cancel-button btn btn-light btn-px-4 py-3 border font-weight-semibold text-color-dark text-3">ยกเลิก</a>
                        </div>
                    </div>
                </div>
            </section>

        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>



</body>

</html>