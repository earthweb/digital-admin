<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>Title Page</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่ม Template</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>จัดการประเภทเอกสาร</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>
                <div class="row">
                    <div class="col">
                        <section class="card">
                            <header class="card-header">
                                <h2 class="card-title">จัดการ Template สำหรับเอกสารประเภท Upload</h2>
                            </header>
                            <div class="card-body">
                                <a type="button" href="1-2-1.php" class=" mb-1 mt-1 mr-1 btn btn-primary btn-px-4 py-3"><i class="fas fa-plus mr-2"></i> สร้าง Template</a>

                                <hr>
                                <table class="table  table-striped mb-0" id="datatable-default">
                                    <thead>
                                        <tr class="head-table">
                                            <th class="center" width="10%">ลำดับ</th>
                                            <th>ชื่อ</th>
                                            <th class="center "  width="10%">แก้ไข</th>
                                            <th class="center "  width="10%">ลบ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="center ">1</td>
                                            <td>
                                            </td>

                                            <td class="actions center">
                                                <a href="#" class="hidden on-editing save-row"><i class="fas fa-save"></i></a>
                                                <a href="#" class="hidden on-editing cancel-row"><i class="fas fa-times"></i></a>
                                                <a href="#" class="on-default edit-row"><i class="fas fa-pencil-alt"></i></a>
                                            </td>
                                            <td class="actions center">
                                                <a href="#" class="on-default remove-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </section>

                    </div>
                </div>

            </section>
        </div>


    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>